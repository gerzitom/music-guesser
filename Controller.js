/*  Less file  
 *  Author: tomasgerzicak
 *  Name: Controller
 *  Version: 1.0
 *  Description:
*/

class Controller
{
    static noPlaylistSelected()
    {
        $(".main-menu__playlist-name").text("No playlist selected");
        $(".main-menu__current-playlist").hide();
    }

    static updatePlaylistPreview(name, id)
    {
        $(".playlist-preview__name").html(name);
        $(".playlist-preview__iframe").attr("src", "https://open.spotify.com/embed?uri=spotify:user:spotify:playlist:"+id);
    }

    static handlebar(templateID, data)
    {
        var template = document.getElementById(templateID).innerHTML;
        let compiledTemplate = Handlebars.compile(template);
        console.log(data);
        return compiledTemplate(data);
    }
}