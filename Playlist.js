/*  Less file  
 *  Author: tomasgerzicak
 *  Name: Playlist
 *  Version: 1.0
 *  Description:
*/

class Playlist
{

    constructor(ID, name, userID)
    {
        this.ID = ID;
        this.name = name;
        this.userID = userID;
    }

    getSongs(accessToken)
    {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: "https://api.spotify.com/v1/users//playlists/" + this.ID +"/tracks",
                headers: {'Authorization': 'Bearer ' + accessToken},
                success: function (playlist)
                {
                    let songs = [];
                    playlist.items.forEach(song => {
                        songs.push(new Song(song.track.name, song.track.uri));
                    });
                    this.songs = songs;
                    resolve(songs);
                }.bind(this),
                error: function (error) {
                    reject(error)
                }
            });
        })
    }

    static getCurrentID()
    {
        return localStorage.getItem("current-playlist");
    }



    static setCurrentPlaylist(playlistID)
    {
        localStorage.setItem("current-playlist", playlistID);
    }

    static async getCurrentPlaylist()
    {
        return await Playlist.get(Playlist.getCurrentID());
    }

    static async get(playlistID, userID)
    {
        return await new Promise((resolve, reject) => {
            $.ajax({
                url: "https://api.spotify.com/v1/users//playlists/" + playlistID +"",
                headers: {'Authorization': 'Bearer ' + User.getAccessToken()},
                success: function (playlist)
                {
                    resolve(playlist);
                }.bind(this),
                error: function (error) {
                    reject(error)
                }
            });
        })
    }
}