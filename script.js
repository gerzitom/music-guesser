/*
    Author: Tomas Gerzicak
    Version: 1.0
    Description:
*/

$(window).load(function (ev) {
    // $(".loading-screen").hide();
});

$(".page").hide();
$(".loading-screen").show();

window.onSpotifyWebPlaybackSDKReady = () => {

    let userPlaylists = null;



    window.addEventListener('DOMContentLoaded', function() {
        console.log('window - DOMContentLoaded - capture'); // 1st
    }, true);

    let user = new User();


    user.login()
    .then(() => {
        $("#main-menu").show();
        console.log(user);
        $("#user-info").html(Controller.handlebar("user-info-template", {name: user.name, img: user.images[0].url}));
        hashChange();
        window.addEventListener("hashchange", hashChange);
        function hashChange() {
            let hash = window.location.hash;
            hash = hash.substr(1, hash.length - 1);
            $(".loading-screen").show();
            $(".page").hide();
            switch (hash)
            {
                case "":
                    let curPlaylist = localStorage.getItem("current-playlist");
                    if(curPlaylist)
                    {
                        Playlist.get(curPlaylist)
                            .then(playlist => {
                                $(".main-menu__playlist-name").text(playlist.name);
                                $(".main-menu__current-playlist").attr("src", `https://open.spotify.com/embed?uri=spotify:user:${user.ID}:playlist:${playlist.id}`)
                                $("#main-menu").show();
                                $(".loading-screen").hide();
                            });
                    }
                    else {
                        Controller.noPlaylistSelected();
                        $(".loading-screen").hide();
                        $("#main-menu").show();
                    }
                    break;
                case "choose-playlist":
                    $("#choose-playlist").show();

                    user.getAllPlaylists()
                        .then(playlists => {
                            let playlistBox = $("#choose-playlist .playlist-box");
                            let currentPlaylistID = Playlist.getCurrentID();
                            if(currentPlaylistID)
                            {
                                Playlist.get(currentPlaylistID, user.ID)
                                    .then(currentPlaylist => {
                                            $(".playlist-preview-box").html(Controller.handlebar("playlist-preview-template", {name: currentPlaylist.name, ID: currentPlaylist.id}));
                                        $(".loading-screen").hide();
                                    })
                            }
                            else $(".loading-screen").hide();

                            playlists.playlists.forEach(playlist => {
                                let selected = "";
                                if(playlist.ID == currentPlaylistID) selected = "button--selected";
                                playlistBox.append(`
                                        <button class="playlist button ${selected}">
                                            <span class="playlist__name">${playlist.name}</span>
                                            <span class="hidden">${playlist.ID}</span>
                                        </button>
                                    `);
                            });
                            $(".playlist").on("click", function(){
                                $(".button").removeClass("button--selected");
                                $(this).addClass("button--selected");
                                let playlistName = $(this).children()[0].innerHTML;
                                let playlistID = $(this).children()[1].innerHTML;
                                Controller.updatePlaylistPreview(playlistName, playlistID);
                                currentPlaylistID = playlistID;
                            });

                            $("#choose-playlist__submit").on("click", function (ev) {
                                window.location.hash = "";
                                Playlist.setCurrentPlaylist(currentPlaylistID);
                            });
                        });
                    break;
                case "option-page":
                    $("#option-page").show();
                    $(".loading-screen").hide();


                    break;
                case "playing":
                    let game = new Game(user);
                    game.init()
                        .then(success => {
                            $("#playing-song").show();
                            $(".loading-screen").hide();
                            $("#score").text(game.data.score);
                            $("#submit-song").on("click", function () {
                                game.submitSong();
                            });
                            $("#next-song").on("click", function () {
                                game.nextSong();
                            });
                            $("#end-game-button").on("click", function () {
                                game.end();
                            })
                        })
                        .catch(err => {
                            console.log(err)
                        })
                    break;
                case "results":
                    $("#results").show();
                    $(".loading-screen").hide();
                    console.log(Results.getAll())
                    $(".results__box").html(Controller.handlebar("results-template", Results.getAll()));
                    break;
            }
        }

    }).catch(err => {
        console.log(err)
    })




// let playlists = [];
//
//
//
//
// const token = User.getAccessToken();
// const player = new Spotify.Player({
//     name: 'Web Playback SDK Quick Start Player',
//     getOAuthToken: cb => {
//         cb(token);
//     }
// });
//
//
// var songInfoTemplate = document.getElementById("song-info").innerHTML;
// songInfoTemplate = Handlebars.compile(songInfoTemplate);
//
// const songTarget = document.getElementById("currently-playing");
//
//
//
//     // Error handling
//     player.addListener('initialization_error', ({ message }) => { console.error(message); });
//     player.addListener('authentication_error', ({ message }) => { console.error(message); });
//     player.addListener('account_error', ({ message }) => { console.error(message); });
//     player.addListener('playback_error', ({ message }) => { console.error(message); });
//
//     // Playback status updates
//     player.addListener('player_state_changed', state => { console.log(state); });
//
//     // Ready
//     player.addListener('ready', ({ device_id }) => {
//         fetch(`https://api.spotify.com/v1/me/player/play?device_id=${device_id}`, {
//             method: 'PUT',
//             body: JSON.stringify({ uris: ["spotify:track:1CrDibvKDE6L47QZW9PoT3", "spotify:track:7lQWRAjyhTpCWFC0jmclT4"] }),
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Authorization': `Bearer ${token}`
//             },
//         });
//         console.log('Ready with Device ID', device_id);
//     });
//
//     player.addListener('player_state_changed', ({track_window: { current_track }}) => {
//         console.log('Currently Playing', current_track);
//         var ctx = {songName: current_track.name, author: current_track.artist};
//         var html = songInfoTemplate(ctx);
//         songTarget.innerHTML = html;
//     });
//
//
//
//     if(false)
//     {
//         player.connect();
//         // player.pause();
//         window.location.hash = "playing";
//     }
};



function parseCookies()
{
    let cookiesText = document.cookie.split("; ")
    let cookies = [];
    cookiesText.forEach(cookie => {
        cookies.push(cookie.split("="));
    });
    return cookies;
}


function getAccessToken()
{
    let accessToken = null;
    document.cookie.split(";").forEach((el) => {
        var cookie = el.trim();
        cookie = cookie.split("=");
        if(cookie[0] == "at")
            accessToken = cookie[1];

    });
    return accessToken;
}
