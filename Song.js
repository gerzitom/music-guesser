/*  Less file  
 *  Author: tomasgerzicak
 *  Name: Song
 *  Version: 1.0
 *  Description:
*/

class Song
{

    constructor(name, uri)
    {
        this.name = name;
        this.uri = uri;
    }
}