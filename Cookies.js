class Cookies
{
    constructor()
    {
    }

    toArray()
    {
        let cookiesText = document.cookie.split("; ")
        let cookies = [];
        cookiesText.forEach(cookie => {
            let cookieArray = cookie.split("=");
            cookies.push({
                name: cookieArray[0],
                value: cookieArray[1]
            });
        });
        return cookies;
    }
    add(cookieName, cookieValue)
    {
        window.cookie = cookieName + "=" + cookieValue;
    }
    remove(cookieName)
    {
        document.cookie = cookieName + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT"
    }
    get(cookieName)
    {
        let cookies = this.toArray();
        let foundCookie = null;
        cookies.forEach(cookie => {
            if(cookie.name == cookieName)
                foundCookie = cookie.value;
        });
        return foundCookie;
    }


    static toArray()
    {
        let cookiesText = document.cookie.split("; ")
        let cookies = [];
        cookiesText.forEach(cookie => {
            let cookieArray = cookie.split("=");
            cookies.push({
                name: cookieArray[0],
                value: cookieArray[1]
            });
        });
        return cookies;
    }


    static get(cookieName)
    {
        let cookies = Cookies.toArray();
        let foundCookie = null;
        cookies.forEach(cookie => {
            if(cookie.name == cookieName)
                foundCookie = cookie.value;
        });
        return foundCookie;
    }
}