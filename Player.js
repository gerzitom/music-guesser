/*  Less file  
 *  Author: tomasgerzicak
 *  Name: Player
 *  Version: 1.0
 *  Description:
*/

class Player
{
    constructor(songs)
    {
        this.songs = songs;
        this.accessToken = User.getAccessToken();


        const token = this.accessToken;

        const player = new Spotify.Player({
            name: 'Web Playback SDK Quick Start Player',
            getOAuthToken: cb => {
                cb(token);
            }
        });


        let songInfoTemplate = $("#song-info-template").html();
        const songInfoHandlebar = Handlebars.compile(songInfoTemplate);

        // Error handling
        player.addListener('initialization_error', ({ message }) => { console.error(message); });
        player.addListener('authentication_error', ({ message }) => { console.error(message); });
        player.addListener('account_error', ({ message }) => { console.error(message); });
        player.addListener('playback_error', ({ message }) => { console.error(message); });

        // Playback status updates
        player.addListener('player_state_changed', state => {
            this.currentSong = state.track_window.current_track;
            console.log(state.track_window.current_track);
        });

        // Ready
        player.addListener('ready', ({ device_id }) => {
            fetch(`https://api.spotify.com/v1/me/player/play?device_id=${device_id}`, {
                method: 'PUT',
                body: JSON.stringify({ uris: this.prepareSongsUris()}),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
            });
        });

        player.addListener('player_state_changed', ({track_window: { current_track }}) => {
            $(".player-box").html(songInfoHandlebar(current_track));
        });

        this.player = player;
    }

    prepareSongsUris()
    {
        let songsUriArray = [];
        this.songs.forEach(song => {
            songsUriArray.push(song.uri);
        });
        return songsUriArray;
    }

    async play()
    {
        return this.player.connect()
            .then(() => {
                this.player.getCurrentState().then(state => {
                    console.log(state)
                })
            })
    }

    async stop()
    {
        return this.player.disconnect();
    }

    pause()
    {
        this.player.pause();
    }
    async nextTrack()
    {
        return await this.player.nextTrack();
    }
}