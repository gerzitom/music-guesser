/*  Less file  
 *  Author: tomasgerzicak
 *  Name: User
 *  Version: 1.0
 *  Description:
*/

class User
{
    constructor()
    {
        this.currentPlaylist = null;
    }

    async login()
    {
        let accessToken = User.getAccessToken();
        return await new Promise((resolve, reject) => {
            $.ajax({
                url: 'https://api.spotify.com/v1/me', headers: {
                    'Authorization': 'Bearer ' + accessToken
                },
                success: function(response)
                {
                    this.ID = response.id;
                    this.name = response.display_name;
                    this.email = response.email;
                    this.images = response.images;
                    this.accessToken = accessToken;
                    resolve(response);
                    // localStorage.setItem("user")
                }.bind(this),
                error: function (err)
                {
                    console.log(err);
                    window.location.href = "index.html";
                    reject(err);
                }
            });
        });
    }

    getCurrentPlaylist()
    {
        let cookies = parseCookies();
        let currentPlaylist = null;
        cookies.forEach(cookie => {
            if(cookie[0] == "current_playlist")
            {
                currentPlaylist = cookie[1];
            }
        });
        return currentPlaylist;
    }

    getAllPlaylists()
    {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'https://api.spotify.com/v1/me/playlists', headers: {
                    'Authorization': 'Bearer ' + this.accessToken
                },
                success: function(response){
                    let userPlaylists = new Playlists();
                    var playlists = [];
                    let i = 0;
                    response.items.forEach(playlist => {
                        let userPlaylist = new Playlist(playlist.id, playlist.name, this.ID);
                        userPlaylist.getSongs(User.getAccessToken())
                            .then(resolveSong =>{
                                userPlaylists.push(userPlaylist);
                                playlists.push(userPlaylist);
                                i++;
                                if(i == response.items.length)
                                    resolve(userPlaylists);
                            });
                    });
                }.bind(this)
            });
        })
    }

    static getAccessToken()
    {
        let accessToken = null;
        document.cookie.split(";").forEach((el) => {
            var cookie = el.trim();
            cookie = cookie.split("=");
            if(cookie[0] == "at")
                accessToken = cookie[1];

        });
        return accessToken;
    }

    static isLogged()
    {
        return (this.getAccessToken()) ? true : false;
    }
}