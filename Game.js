/*  Less file  
 *  Author: tomasgerzicak
 *  Name: Game
 *  Version: 1.0
 *  Description:
*/

class Game
{
    constructor(user)
    {
        this.user = user;
        this.player = null;
        this.curSongs = [];
    }

    getScore()
    {
        let score = 0;
        this.data.songs.forEach(song => {
            score += parseInt(song.score.artist);
            score += parseInt(song.score.name)
        });
        return score;
    }


    async init()
    {
        this.data = GameData.load();
        if (!this.data) {
            this.data = new GameData(this.user);
            let playlist = await Playlist.getCurrentPlaylist();
            this.data.songsCount = playlist.tracks.items.length;
            let songs = [];
            playlist.tracks.items.forEach((song, i) => {
                songs.push({
                    name: song.track.name,
                    artist: song.track.artists[0].name,
                    uri: song.track.uri,
                    score: {name: 0, artist: 0}})
            });
            this.data.songs = songs;
            GameData.save(this.data);
            console.log(this.data)
        }
        this.data.songs.forEach((song, i) => {
            if(i >= this.data.currentSong) this.curSongs.push(song);
        });
        this.player = new Player(this.curSongs);
        this.currentSongScore = {};
        $("#score").text(this.getScore());
        if(this.data.currentSong == this.data.songs.length - 1)
        {
            $("#next-song").text("Finish");
        }
        return this.player.play()
    }

    submitSong()
    {
        var songName = $("#song-name").val();
        var author = $("#song-author").val();
        this.currentSongScore = this.validateInput(this.player.currentSong, {name: songName, artist: author});
        this.data.songs[this.data.currentSong].score.name = this.currentSongScore.name;
        this.data.songs[this.data.currentSong].score.artist = this.currentSongScore.artist;
        $("#score").text(this.getScore());
        console.log(this.data)
        GameData.save(this.data);
    }


    nextSong()
    {
        this.submitSong();
        this.currentSongScore = {name: 0, artist: 0};
        $("#score").text(this.getScore());
        if(this.data.currentSong == this.data.songs.length - 2)
        {
            $("#next-song").text("Finish");
        }
        if(this.data.currentSong == this.data.songs.length - 1)
        {
            this.end();
        }
        else
        {
            this.data.currentSong++;
            console.log(this.data);
            GameData.save(this.data);
        }
        this.player.nextTrack().then(() => {
            this.resetInputs();
            console.log("skipped");
        })
    }

    end()
    {
        $(".page").hide();
        $("#end-game").show();
        $("#end-game__game-info").html(Controller.handlebar("end-game-template", {score: this.getScore()}));
        $("#end-game-finish-button").on("click", function () {
            window.location.href = "index.html";
        });
        Results.add({
            user: {
                ID: this.user.ID,
                name: this.user.name
            },
            songs: this.data.songs
        });
        this.data = {};
        GameData.remove();
        this.player.stop();
    }

    validateInput(currentSong, userInput)
    {
        let ret = {
            name: 0,
            artist: 0
        };
        let curSongName = this.normalizeSongInfo(currentSong.name);
        let inputName = this.normalizeSongInfo(userInput.name);
        console.log(curSongName + " == " + inputName);
        if(curSongName !== inputName)
        {
            this.markWrongInput("#song-name");
        }
        else
        {
            this.markRightInput("#song-name");
            ret.name = 5;
        }
        let curSongArtist = this.parseSongName(currentSong.artists[0].name);
        let inputArtist = this.parseSongName(userInput.artist);
        console.log(curSongArtist + " == " + inputArtist);
        if(curSongArtist !== inputArtist)
        {
            this.markWrongInput("#song-author");
        }
        else
        {
            this.markRightInput("#song-author");
            ret.artist = 5;
        }
        return ret;
    }
    parseSongName(songName)
    {
        let parsedInput = songName.replace(/ *\([^)]*\) */g, "");
        let index = parsedInput.indexOf("-");
        if(index != -1)
            parsedInput = parsedInput.substring(0, index);
        return parsedInput.toLowerCase().trim();
    }

    normalizeSongInfo(text)
    {
        let parsedInput = text.replace(/ *\([^)]*\) */g, "");
        let index = parsedInput.indexOf("-");
        if(index != -1)
            parsedInput = parsedInput.substring(0, index);
        return parsedInput.toLowerCase().trim();
    }

    markWrongInput(selector)
    {
        $(selector).css("border-color", "red");
    }

    markRightInput(selector)
    {
        $(selector).css("border-color", "#1DB954");
    }

    resetInputs()
    {
        $("#playing-song input").val("");
        $("#playing-song input").css("border-color", "#1DB954");
    }
}