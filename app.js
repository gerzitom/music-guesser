
const port = 3000;


var client_id = '45c5fb1b98f1406696aaf25f3b7b47ec'; // Your client id
var client_secret = '3177c37f60d8473ea0ba9721c39379b7'; // Your secret
var redirect_uri = 'http://localhost:3000/callback'; // Your redirect uri

const express = require("express");
const handlebars = require("handlebars");
const request = require("request");
const querystring = require("querystring");
const app = express();

app.use(express.static(__dirname + '/'));


var generateRandomString = function(length) {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
};

var stateKey = 'spotify_auth_state';


app.get("/login", (req, res) => {

    var state = generateRandomString(16);
    res.cookie(stateKey, state);
    var scope = 'user-read-private user-read-email user-read-birthdate streaming user-read-playback-state user-read-currently-playing user-read-recently-played user-modify-playback-state';
    res.redirect('https://accounts.spotify.com/authorize?' +
        querystring.stringify({
            response_type: 'code',
            client_id: client_id,
            scope: scope,
            redirect_uri: redirect_uri,
            state: state
        }));

    var sth = querystring.stringify({
        response_type: 'code',
        client_id: client_id,
        scope: scope,
        redirect_uri: redirect_uri,
        state: state
    });
    console.log(sth);
});

app.get("/callback", (req, res) => {
    const code = req.query.code || null;
    var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        form: {
            code: code,
            redirect_uri: redirect_uri,
            grant_type: 'authorization_code'
        },
        headers: {
            'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
        },
        json: true
    };
    request.post(authOptions, (error, response, body) => {
        console.log(response);
        res.cookie("access-token", body.access_token);
        res.cookie("at", body.access_token);
        res.redirect("http://localhost:3000")
    })
})

app.listen(port, () => {
    console.log("Listening on " + port);
});