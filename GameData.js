/*  Less file  
 *  Author: tomasgerzicak
 *  Name: GameData
 *  Version: 1.0
 *  Description:
*/

class GameData
{

    constructor(user)
    {
        this.user = user;
        this.songs = [];
        this.currentSong = 0;
    }

    static save(data)
    {
        localStorage.setItem("game-data", JSON.stringify(data));
    }

    static load()
    {
        return JSON.parse(localStorage.getItem("game-data"));
    }

    static remove()
    {
        localStorage.removeItem("game-data");
    }
}