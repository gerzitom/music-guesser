/*  Less file  
 *  Author: tomasgerzicak
 *  Name: Results
 *  Version: 1.0
 *  Description:
*/

class Results
{
    static add(result)
    {
        let results = Results.getAll();
        results.results.push(result);
        Results.save(results);
    }

    static getAll()
    {
        // localStorage.setItem("results", JSON.stringify({results: []}));
        let results = localStorage.getItem("results");
        if(!results)
        {
            localStorage.setItem("results", JSON.stringify({results: []}));
            results = localStorage.getItem("results");
        }
        return JSON.parse(results)
    }

    static save(results)
    {
        localStorage.setItem("results", JSON.stringify(results));
    }
}