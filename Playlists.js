/*  Less file  
 *  Author: tomasgerzicak
 *  Name: Playlists
 *  Version: 1.0
 *  Description:
*/

class Playlists
{

    constructor(playlists = [])
    {
        this.playlists = playlists;
    }
    push(playlist)
    {
        this.playlists.push(playlist);
    }
    findPlaylistByID(playlistID)
    {
        let foundPlaylist = null;
        this.playlists.forEach(playlist => {
            if(playlist.ID == playlistID)
            {
                foundPlaylist = playlist;
            }
        });
        return foundPlaylist;
    }
}